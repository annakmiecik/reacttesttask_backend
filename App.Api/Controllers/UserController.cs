﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using App.Common.Authentication;
using App.Common.CQRS.Commands.CommandBus;
using App.Common.CQRS.Query;
using App.Common.RabbitMq;
using App.Common.Settings;
using App.Infrastucture.DTO;
using App.Infrastucture.Messages.Commands.User;
using App.Infrastucture.Query;
using App.Infrastucture.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace App.Api.Controllers
{
    //[Authorize(Roles = "Manager,Administrator")]
    [JwtAuth]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ApiControllerBaseController
    {
        private readonly MongoDbOptions _mongoOptions;
        private readonly IUserService _userService;
        public UserController(IUserService userService, MongoDbOptions mongoOptions, IBusPublisher busPublisher, ICommandBus commandBus, IQueryBus queryBus) : base(busPublisher, commandBus, queryBus)
        {
            _mongoOptions = mongoOptions;
            _userService = userService;
        }



        // GET api/values
        // GET api/test
        [HttpGet]
        public async Task<ActionResult<IEnumerable<string>>> GetAsync()
        {
            var users = await _userService.BrowseAsync();
            return new JsonResult(users);
        }

        [HttpGet("{username}")]
        public async Task<IActionResult> GetAsync(string username)
        {
            //var user = await _userService.GetAsync(username);
            var user = await GetQueryAsync<UserDto, LoadUserQuery>(new LoadUserQuery(username));
            if (user == null)
            {
                return new NotFoundResult();
            }

            return new JsonResult(user);
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] CreateUserCommand command)
        {
            await SendCommandByRabbitMqAsync(command, CorrelationContext.Empty);
            return new CreatedResult($"users/{command.Email}", null);
        }

        // PUT api/values/5
        [HttpPut]
        public async Task PutAsync( [FromBody] UpdateUserCommand command)
        {
            await SendCommandByRabbitMqAsync(command, CorrelationContext.Empty);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task DeleteAsync(Guid id)
        {
            await _userService.RemoveAsync(id);
        }
    }
}