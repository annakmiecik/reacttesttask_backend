﻿using System.Threading.Tasks;
using App.Common.Authentication;
using App.Common.CQRS.Commands.CommandBus;
using App.Common.CQRS.Query;
using App.Common.RabbitMq;
using App.Infrastucture.Messages.Commands.User;
using App.Infrastucture.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace App.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ApiControllerBaseController
    {

        private readonly IIdentityService _identityService;

        private readonly IUserService _userService;
        public AccountController(IIdentityService identityService,
            IUserService userService, IBusPublisher busPublisher, ICommandBus commandBus, IQueryBus queryBus
            ) : base(busPublisher, commandBus, queryBus)
        {
            _identityService = identityService;
            _userService = userService;
        }

        [AllowAnonymous]
        [HttpPut]
        [Route("login")]
        public async Task<JsonWebToken> LoginAsync([FromBody]LoginCommand command)
        {
            //   await SendCommandByInternalCommandBusAsync(comand);

            //await SendCommandByRabbitMqAsync(command, CorrelationContext.Empty);
            return await _identityService.LoginAsync(command.login, command.password);
        }

        [JwtAuth]
        [HttpPut]
        [Route("changePassword")]
        public async Task ChangePasswordAsync([FromBody]ChangeUserPasswordCommand comand)
        {
            await SendCommandByRabbitMqAsync(comand, CorrelationContext.Empty);
            //await SendCommandByInternalCommandBusAsync(comand);
        }


    }
}