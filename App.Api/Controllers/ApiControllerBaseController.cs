﻿using System.Threading.Tasks;
using App.Common.CQRS.Commands;
using App.Common.CQRS.Commands.CommandBus;
using App.Common.CQRS.Query;
using App.Common.RabbitMq;
using App.Common.Types;
using Microsoft.AspNetCore.Mvc;

namespace App.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class ApiControllerBaseController : Controller
    {
        private readonly IBusPublisher _busPublisher;
        private readonly ICommandBus _commandBus;
        private readonly IQueryBus _queryBus;

        public ApiControllerBaseController(IBusPublisher busPublisher, ICommandBus commandBus, IQueryBus queryBus)
        {
            _busPublisher = busPublisher;
            _commandBus = commandBus;
            _queryBus = queryBus;
        }

        public async Task SendCommandByRabbitMqAsync<T>(T command, ICorrelationContext context) where T : ICommand
        {
            await _busPublisher.SendAsync<T>(command, context);
        }

        public async Task SendCommandByInternalCommandBusAsync<T>(T command) where T : ICommand
        {
            await _commandBus.SendAsync<T>(command);
        }

        public async Task<Result> GetQueryAsync<Result, Query>(Query query) where Query: IQuery<Result>
        {
            return await _queryBus.QueryAsync<Result, Query>(query);
        }
    }
}