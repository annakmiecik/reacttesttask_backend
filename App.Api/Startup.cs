﻿using System;
using App.Common.Authentication;
using App.Common.CQRS.Query;
using App.Common.Mongo;
using App.Common.RabbitMq;
using App.Common.Types;
using App.Common.Utils;
using App.Core.Domain;
using App.Infrastucture.DTO;
using App.Infrastucture.IOC;
using App.Infrastucture.Messages.Commands.User;
using App.Infrastucture.Messages.Events;
using App.Infrastucture.Query;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace App
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile("appsettings.docker.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            this.Configuration = builder.Build();
        }

        //public Startup(IConfiguration configuration)
        //{
        //    Configuration = configuration;
        //}

        public IConfigurationRoot Configuration { get; }
        public IContainer ApplicationContainer { get; private set; }

        //public IConfiguration Configuration { get; }

        //// This method gets called by the runtime. Use this method to add services to the container.
        //public void ConfigureServices(IServiceCollection services)
        //{
        //    services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        //    services.AddSingleton(AutoMapperConfig.Initialize());
        //    services.AddScoped<IUserService, UserService>();
        //   // services.AddScoped<IUserRepository, UserRepository>();


        //    //// Add configuration for DbContext
        //    //// Use connection string from appsettings.json file
        //    //services.AddDbContext<WideWorldImportersDbContext>(options =>
        //    //{
        //    //    options.UseSqlServer(Configuration["AppSettings:ConnectionString"]);
        //    //});

        //    //// Set up dependency injection for controller's logger
        //    //services.AddScoped<ILogger, Logger<WarehouseController>>();
        //}

        // ConfigureServices is where you register dependencies. This gets
        // called by the runtime before the Configure method, below.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // Add services to the collection.
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddHttpsRedirection(options =>
            {
                options.RedirectStatusCode = StatusCodes.Status307TemporaryRedirect;
                options.HttpsPort = 44381;
            });

            services.AddJwt();

            // Create the container builder.
            var builder = new ContainerBuilder();

            //builder.RegisterAssemblyTypes(Assembly.GetEntryAssembly()).AsImplementedInterfaces();

            // Register dependencies, populate the services from
            // the collection, and build the container. If you want
            // to dispose of the container at the end of the app,
            // be sure to keep a reference to it as a property or field.
            builder.Populate(services);
            builder.RegisterModule(new ContainerModule(Configuration));

            builder.AddPrimitiveCQRS(typeof(ContainerModule));

            builder.AddRabbitMq();
            builder.AddMongo();

            //builder.AddMongoRepository<RefreshToken>("RefreshTokens");
            builder.AddMongoRepository<User>("Users");
            builder.AddInitializers(typeof(MongoDbInitializer));

            this.ApplicationContainer = builder.Build();

            // Create the IServiceProvider based on the container.
            return new AutofacServiceProvider(this.ApplicationContainer);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IStartupInitializer initializer, IApplicationLifetime appLifetime)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseCors(builder =>
                builder.WithOrigins("http://localhost:3000")
                       .AllowAnyHeader().AllowAnyMethod()
                );

            app.UseHttpsRedirection();
            App.Api.Framework.Extensions.UseExceptionHandler(app);
            // app.UseExceptionHandler("/Error");
            app.UseAuthentication();
            app.UseAccessTokenValidator();
            app.UseMvc();
            app.UseHttpsRedirection();
            app.UseRabbitMq()
                .SubscribeCommand<LoginCommand>()
                .SubscribeCommand<ChangeUserPasswordCommand>()
                .SubscribeEvent<UserPasswordChangedEvent>()
                .SubscribeCommand<CreateUserCommand>()
                .SubscribeCommand<UpdateUserCommand>();

            appLifetime.ApplicationStopped.Register(() => this.ApplicationContainer.Dispose());

            var t = initializer.InitializeAsync();
            t.Wait();
        }
    }
}
