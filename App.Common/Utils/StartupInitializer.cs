﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Common.Utils
{
    public class StartupInitializer : IStartupInitializer
    {
        private HashSet<IInitializer> _initializers = new HashSet<IInitializer>();

        public void AddInitializer(IInitializer initializer)
        {
            if (initializer != null)
            {
                _initializers.Add(initializer);
            }
        }

        public Task InitializeAsync() => Task.WhenAll(_initializers.Select(i=>i.InitializeAsync()));
    }
}
