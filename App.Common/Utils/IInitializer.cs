﻿using System.Threading.Tasks;

namespace App.Common.Utils
{
    public interface IInitializer
    {
        Task InitializeAsync();
    }
}
