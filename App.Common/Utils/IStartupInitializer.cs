﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace App.Common.Utils
{
    public interface IStartupInitializer
    {
        Task InitializeAsync();
        void AddInitializer(IInitializer initializer);
    }
}
