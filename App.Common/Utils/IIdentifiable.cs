﻿using System;

namespace App.Common.Utils
{
    public interface IIdentifiable
    {
        Guid Id { get; }
    }
}
