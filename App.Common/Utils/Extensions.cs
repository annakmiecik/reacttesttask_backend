﻿using App.Common.Mongo;
using App.Common.Settings;
using Autofac;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace App.Common.Utils
{
    public static class Extensions
    {
        public static void AddInitializers(this ContainerBuilder builder, params Type[] initializers)
        {
            builder.Register(context =>
            {
                var ctx = context.Resolve<IComponentContext>();
                var startupInitializer = new StartupInitializer(); 
                var validInitializers = initializers.Where(t => typeof(IInitializer).IsAssignableFrom(t));

                foreach (var initializer in validInitializers)
                {
                    startupInitializer.AddInitializer(context.Resolve(initializer.GetInterface("IInitializer")) as IInitializer);
                }

                return startupInitializer;

            })
            .AsImplementedInterfaces()
            .InstancePerLifetimeScope();
        }
       
    }
}
