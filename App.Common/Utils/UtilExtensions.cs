﻿using System.Linq;

namespace App.Common.Utils
{
    public static class UtilExtensions
    {
         
        public static string Underscore(this string value)
            => string.Concat(value.Select((x, i) => i > 0 && char.IsUpper(x) ? "_" + x.ToString() : x.ToString()));

    }
}
