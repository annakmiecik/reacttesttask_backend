﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace App.Common.Settings
{
    public interface IMongoDbSeeder
    {
        Task SeedAsync();
    }
}
