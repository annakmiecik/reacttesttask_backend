﻿using App.Common.Settings;
using App.Common.Utils;
using Autofac;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace App.Common.Mongo
{
    public static class Extensions
    {
        public static void AddMongo(this ContainerBuilder builder)
        {
            builder.Register(context =>
            {
                var configuration = context.Resolve<IConfiguration>();
                var t1 = configuration.GetSection("mongo");
                var options = configuration.GetOptions<MongoDbOptions>("mongo");

                return options;
            }).SingleInstance();

            builder.Register(context =>
            {
                var options = context.Resolve<MongoDbOptions>();

                return new MongoClient(options.ConnectionString);
            }).SingleInstance();

            builder.Register(context =>
            {
                var options = context.Resolve<MongoDbOptions>();
                var client = context.Resolve<MongoClient>();
                return client.GetDatabase(options.Database);

            }).InstancePerLifetimeScope();

            var assembly = typeof(Extensions)
                .GetTypeInfo()
                .Assembly;

            builder.RegisterAssemblyTypes(assembly)
                   .Where(x => x.IsAssignableTo<IInitializer>())
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();
        }

        public static void AddMongoRepository<TEntity>(this ContainerBuilder builder, string collectionName)
            where TEntity : IIdentifiable
            => builder.Register(ctx => new MongoRepository<TEntity>(ctx.Resolve<IMongoDatabase>(), collectionName))
                .As<IMongoRepository<TEntity>>()
                .InstancePerLifetimeScope();

        //public static void AddUserSeeder(this ContainerBuilder builder)
        //{
        //    builder.RegisterType<MongoDbInitializer>()
        //        // .As<IMongoDbInitializer>()
        //        .As<IInitializer>()
        //        .InstancePerLifetimeScope();

        //    builder.RegisterType<StartupInitializer>()
        //        .As<IStartupInitializer>()
        //        .InstancePerLifetimeScope();
        //}
    }
}
