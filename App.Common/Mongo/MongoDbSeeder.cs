﻿using MongoDB.Driver;
using System.Linq;
using System.Threading.Tasks;

namespace App.Common.Settings
{
    public abstract class MongoDbSeeder : IMongoDbSeeder
    {
        protected readonly IMongoDatabase Database;

        public MongoDbSeeder(IMongoDatabase database)
        {
            Database = database;
        }

        public async Task SeedAsync()
        {
            await CustomSeedAsync();
        }

        protected virtual async Task<bool> CustomSeedAsync()
        {
            var cursor = await Database.ListCollectionsAsync();
            var collections = await cursor.ToListAsync();
            return collections.Any();
        }


    }
}
