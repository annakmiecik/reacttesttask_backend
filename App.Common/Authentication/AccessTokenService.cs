﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Common.Authentication
{
    public class AccessTokenService : IAccessTokenService
    {
        //private readonly IDistributedCache _cache;
        private readonly IJwtHandler _jwtHandler;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IOptions<JwtOptions> _jwtOptions;

        public AccessTokenService(/*IDistributedCache cache,*/
                IHttpContextAccessor httpContextAccessor,
                IJwtHandler jwtHandler,
                IOptions<JwtOptions> jwtOptions)
        {
           // _cache = cache;
            _httpContextAccessor = httpContextAccessor;
            _jwtHandler = jwtHandler;
            _jwtOptions = jwtOptions;
        }

        public Task DeactivateAsync(string userId, string token)
        {
            throw new NotImplementedException();
        }

        public async Task DeactivateCurrentAsync(string userId)
        {
            var currentToken = GetCurrent();
            await DeactivateAsync(userId, currentToken);
        }

        public async Task<bool> IsActiveAsync(string token)
        {
            //pobrać z cacheu lub repozytorium Mongo i sprawdzić
            // throw new NotImplementedException();

            var payload = _jwtHandler.GetTokenPayload(token);

            return await Task.FromResult(true);
        }

        public async Task<bool> IsCurrentActiveToken()
        {
            var currentToken = GetCurrent();
            return await IsActiveAsync(currentToken);
        }

        private string GetCurrent()
        {
            var authorizationHeader = _httpContextAccessor
                .HttpContext.Request.Headers["authorization"];

            return authorizationHeader == StringValues.Empty
                ? string.Empty
                : authorizationHeader.Single().Split(' ').Last();
        }

        private static string GetKey(string token)
            => $"tokens:{token}";
    }
}
