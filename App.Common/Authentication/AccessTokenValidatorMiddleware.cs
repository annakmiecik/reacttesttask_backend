﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System.Net;
using System.Threading.Tasks;

namespace App.Common.Authentication
{
    public class AccessTokenValidatorMiddleware : IMiddleware
    {
        private readonly IAccessTokenService _accessTokenService;
        private readonly IAuthenticationSchemeProvider _schemes;

        public AccessTokenValidatorMiddleware(IAccessTokenService accessTokenService, IAuthenticationSchemeProvider schemes)
        {
            _accessTokenService = accessTokenService;
            _schemes = schemes;
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            var defaultAuthenticate = await _schemes.GetDefaultAuthenticateSchemeAsync();
            if (defaultAuthenticate != null)
            {
                var result = await context.AuthenticateAsync(defaultAuthenticate.Name);
                if (result != null && result.Succeeded)
                {
                    if (await _accessTokenService.IsCurrentActiveToken())
                    {
                        await next(context);

                        return;
                    }
                    context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                }
            }
            await next(context);
        }
    }
}
