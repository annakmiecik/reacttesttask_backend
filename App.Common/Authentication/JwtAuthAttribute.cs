﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace App.Common.Authentication
{
    public class JwtAuthAttribute : AuthorizeAttribute
    {
        public JwtAuthAttribute(string policy = "", string scheme = JwtBearerDefaults.AuthenticationScheme)
        : base(policy)
        {
            AuthenticationSchemes = scheme;
        }
    }
}
