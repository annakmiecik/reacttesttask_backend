﻿using App.Infrastucture.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Common.Messages
{
    public interface IRejectedEvent : IEvent
    {
        string Reason { get; }
        string Code { get; }
    }
}
