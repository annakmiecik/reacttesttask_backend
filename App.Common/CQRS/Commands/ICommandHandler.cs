﻿using App.Common.RabbitMq;
using System.Threading.Tasks;

namespace App.Common.CQRS.Commands
{
    public interface ICommandHandler { }
    public interface ICommandHandler<TCommand> : ICommandHandler where TCommand : ICommand 
    {
        Task HandleAsync(TCommand command);

        Task HandleAsync(TCommand command, ICorrelationContext context);
    }
}
