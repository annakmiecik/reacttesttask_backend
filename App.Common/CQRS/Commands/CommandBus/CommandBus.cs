﻿using System;
using System.Threading.Tasks;

namespace App.Common.CQRS.Commands.CommandBus
{
    public interface ICommandBus
    {
        Task SendAsync<T>(T command) where T : ICommand;
    }

    public class CommandBus : ICommandBus
    {
        private readonly Func<Type, ICommandHandler> _handlersFactory;
        public CommandBus(Func<Type, ICommandHandler> handlersFactory)
        {
            _handlersFactory = handlersFactory;
        }

        public async Task SendAsync<TCommand>(TCommand command) where TCommand : ICommand
        {
            var handler = (ICommandHandler<TCommand>)_handlersFactory(typeof(TCommand));
            await handler.HandleAsync(command);
        }
    }
}
