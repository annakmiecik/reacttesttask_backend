﻿using App.Infrastucture.Commands;
using App.Infrastucture.Commands.EventBus;
using Autofac;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace App.Common.CQRS.IOC.Modules
{
    public class EventsModule : Autofac.Module
    {
        private readonly Assembly[] _assemblies;
        public EventsModule(params Assembly[] assemblies)
        {
            _assemblies = assemblies;
        }

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            //ThisAssembly
            builder.RegisterAssemblyTypes(_assemblies)
                .Where(x => x.IsAssignableTo<IHandleEvent>())
                .AsImplementedInterfaces();

            builder.Register<Func<Type, IEnumerable<IHandleEvent>>>(c =>
            {
                var ctx = c.Resolve<IComponentContext>();
                return t =>
                {
                    var handlerType = typeof(IHandleEvent<>).MakeGenericType(t);
                    var handlersCollectionType = typeof(IEnumerable<>).MakeGenericType(handlerType);
                    return (IEnumerable<IHandleEvent>)ctx.Resolve(handlersCollectionType);
                };
            });

            builder.RegisterType<EventsBus>()
                .AsImplementedInterfaces();
        }
    }

}
