﻿using App.Common.CQRS.Commands;
using App.Common.CQRS.Commands.CommandBus;
using Autofac;
using System;
using System.Reflection;

namespace App.Common.CQRS.IOC.Modules
{
    public class CommandModule : Autofac.Module
    {
        private readonly Assembly[] _assemblies;
        public CommandModule(params Assembly[] assemblies)
        {
            _assemblies = assemblies;
        }

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            //ThisAssembly
            builder.RegisterAssemblyTypes(_assemblies)
                .Where(x => x.IsAssignableTo<ICommandHandler>())
                .AsImplementedInterfaces();

            builder.Register<Func<Type, ICommandHandler>>(c =>
            {
                var ctx = c.Resolve<IComponentContext>();

                return t =>
                {
                    var handlerType = typeof(ICommandHandler<>).MakeGenericType(t);
                    return (ICommandHandler)ctx.Resolve(handlerType);
                };
            });

            builder.RegisterType<CommandBus>()
                .AsImplementedInterfaces();
        }
    }
}
