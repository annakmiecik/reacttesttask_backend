﻿using App.Common.CQRS.Query;
using App.Common.Types;
using Autofac;
using System;
using System.Reflection;

namespace App.Common.CQRS.IOC.Modules
{
    public class QueryModule : Autofac.Module
    {
        private readonly Assembly[] _assemblies;
        public QueryModule(params Assembly[] assemblies)
        {
            _assemblies = assemblies;
        }

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            //ThisAssembly

            builder.RegisterAssemblyTypes(_assemblies)
           .Where(x => x.IsAssignableTo<IQuery>())
           .AsImplementedInterfaces();

            //builder.RegisterAssemblyTypes(_assemblies)
            //    .Where(x => x.IsAssignableTo<IQueryHandler>())
            //    .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(_assemblies)
                .AsClosedTypesOf(typeof(IQueryHandler<,>));

            builder.Register<Func<Type, IQueryHandler>>(c =>
            {
                var ctx = c.Resolve<IComponentContext>();

                return t =>
                {
                    var queryType = typeof(IQueryHandler<,>).MakeGenericType(t, t.GetInterfaces()[0].GenericTypeArguments[0]);
                    var solution = queryType;
                    return (IQueryHandler)ctx.Resolve(queryType);
                };
            });

            builder.RegisterType<QueryBus>()
                .AsImplementedInterfaces();
        }
    }
}
