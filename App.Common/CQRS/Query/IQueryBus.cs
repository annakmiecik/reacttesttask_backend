﻿using App.Common.Types;
using System.Threading.Tasks;

namespace App.Common.CQRS.Query
{
    public interface IQueryBus
    {
        Task<TResult> QueryAsync<TResult, Query>(Query query) where Query : IQuery<TResult>;
    }
}
