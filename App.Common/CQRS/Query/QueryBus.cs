﻿using App.Common.Types;
using System;
using System.Threading.Tasks;

namespace App.Common.CQRS.Query
{
    public class QueryBus : IQueryBus
    {
        public async Task<TResult> QueryAsync<TResult, Query>(Query query) where Query : IQuery<TResult>
        {
            var handler = (IQueryHandler<Query, TResult >)_handlersFactory(typeof(Query));
            return await handler.HandleAsync(query);
        }

        private readonly Func<Type, IQueryHandler> _handlersFactory;
        public QueryBus(Func<Type, IQueryHandler> handlersFactory)
        {
            _handlersFactory = handlersFactory;
        }

    }
}
