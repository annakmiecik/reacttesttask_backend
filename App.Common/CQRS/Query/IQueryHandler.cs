﻿using App.Common.Types;
using System.Threading.Tasks;

namespace App.Common.CQRS.Query
{
    public interface IQueryHandler
    {

    }

    public interface IQueryHandler<TQuery, TResult> : IQueryHandler where TQuery : IQuery<TResult>
    {
        Task<TResult> HandleAsync(TQuery query);
    }
}
