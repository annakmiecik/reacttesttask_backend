﻿using App.Common.RabbitMq;
using System.Threading.Tasks;

namespace App.Infrastucture.Commands
{
    public interface IEvent
    {

    }

    public interface IHandleEvent
    {

    }

    public interface IHandleEvent<TEvent> : IHandleEvent
        where TEvent : IEvent
    {
        Task HandleAsync(TEvent @event);

        Task HandleAsync(TEvent @event, ICorrelationContext context);
    }
}
