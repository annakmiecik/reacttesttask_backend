﻿using System.Linq;
using Microsoft.Extensions.Configuration;

namespace App.Common.Settings
{
    public static class SettingsExtensions
    {
        public static TModel GetOptions<TModel>(this IConfiguration configuration, string section) where TModel : new()
        {
            var model = new TModel();
            (configuration.GetSection(section) as IConfiguration).Bind(model);

            return model;
        }
    }
}
