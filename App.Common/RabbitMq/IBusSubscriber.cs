﻿using App.Common.CQRS.Commands;
using App.Common.Messages;
using App.Common.Types;
using App.Infrastucture.Commands;
using System;

namespace App.Common.RabbitMq
{
    public interface IBusSubscriber
    {
        IBusSubscriber SubscribeCommand<TCommand>(string @namespace = null, string queueName = null,
            Func<TCommand, AppException, IRejectedEvent> onError = null)
            where TCommand : ICommand;

        IBusSubscriber SubscribeEvent<TEvent>(string @namespace = null, string queueName = null,
            Func<TEvent, AppException, IRejectedEvent> onError = null)
            where TEvent : IEvent;
    }
}
