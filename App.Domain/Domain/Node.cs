﻿using System;

namespace App.Core.Domain
{
    public class Node
    {
        public Guid Id { get; protected set; }
        public string Address { get; protected set; }
    }
}
