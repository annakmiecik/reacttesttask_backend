﻿using App.Common.Authentication;
using System;
using System.Threading.Tasks;

namespace App.Infrastucture.Services
{
    public interface IIdentityService : IService
    {
        Task ChangePasswordAsync(Guid userId, string currentPassword, string newPassword);
        Task<JsonWebToken> LoginAsync(string login, string password);
        Task LogoutAsync(string login, string password);
    }
}