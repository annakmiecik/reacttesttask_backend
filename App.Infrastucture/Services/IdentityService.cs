﻿using App.Common.Authentication;
using App.Common.Settings;
using App.Core.Domain;
using App.Infrastucture.Exceptions;
using AutoMapper;
using System;
using System.Threading.Tasks;

namespace App.Infrastucture.Services
{
    public class IdentityService : IIdentityService
    {
        private readonly IClaimsProvider _claimsProvider;
        private readonly IJwtHandler _jwtHandler;
        private readonly IEncrypter _encrypter;
        private readonly IMapper _mapper;
        private readonly IMongoRepository<User> _userRepository;

        public IdentityService(IMongoRepository<User> userRepository, IEncrypter encrypter, IClaimsProvider claimsProvider, IJwtHandler jwtHandler, IMapper mapper)
        {
            _claimsProvider = claimsProvider;
            _encrypter = encrypter;
            _jwtHandler = jwtHandler;
            _mapper = mapper;
            _userRepository = userRepository;
        }

        public async Task ChangePasswordAsync(Guid userId, string currentPassword, string newPassword)
        {
            var userTest = await _userRepository.FirstOrDefaultAsync(u => u.Username == "ankm");


            var user = await _userRepository.GetAsync(userId);
            if (user == null)
            {
                throw new ServiceException(Exceptions.ErrorCodes.UserNotFound,
                    "User not found");
            }

            var hash = _encrypter.GetHash(currentPassword, user.Salt);
            if (user.Password == hash)
            {
                user.SetPassword(_encrypter.GetHash(newPassword, user.Salt), user.Salt);
                await _userRepository.UpdateAsync(user);
                return;
            }

            throw new ServiceException(Exceptions.ErrorCodes.InvalidCredentials,
                   "Invalid previous password");
        }

        public async Task<JsonWebToken> LoginAsync(string login, string password)
        {
            var user = await _userRepository.FirstOrDefaultAsync(u => u.Username == login);
            if (user == null)
            {
                throw new ServiceException(Exceptions.ErrorCodes.InvalidCredentials,
                    "Invalid credentials");
            }

            var hash = _encrypter.GetHash(password, user.Salt);
            if (user.Password == hash)
            {
                //var refreshToken = new RefreshToken(user, _passwordHasher);
                var claims = await _claimsProvider.GetAsync(login);
                var jwt = _jwtHandler.CreateToken(user.Id.ToString("N"), user.Role.ToString(), claims);
                //jwt.RefreshToken = refreshToken.Token;
                //await _refreshTokenRepository.AddAsync(refreshToken);

                return jwt;
            }
            throw new ServiceException(Exceptions.ErrorCodes.InvalidCredentials,
                "Invalid credentials");
        }

        public Task LogoutAsync(string login, string password)
        {
            throw new NotImplementedException();
        }
    }
}
