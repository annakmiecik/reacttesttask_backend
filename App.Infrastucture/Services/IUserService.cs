﻿using App.Core.Domain;
using App.Infrastucture.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace App.Infrastucture.Services
{
    public interface IUserService : IService
    {
        Task<IEnumerable<UserDto>> BrowseAsync();
        Task<UserDto> GetAsync(string username);
        Task RegisterAsync(Guid userId, string email,
                    string username, string fullName, string password, UserRole role);
        Task RemoveAsync(Guid id);
        Task UpdateAsync(UserDto user);
    }
}
