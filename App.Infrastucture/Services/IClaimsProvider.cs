﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace App.Infrastucture.Services
{
    public interface IClaimsProvider : IService
    {
        Task<Dictionary<string, string>> GetAsync(string userName);
    }
}
