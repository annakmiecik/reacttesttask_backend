﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using App.Common.Settings;
using App.Core.Domain;
using App.Infrastucture.DTO;
using App.Infrastucture.Exceptions;
using AutoMapper;

namespace App.Infrastucture.Services
{
    public class UserService : IUserService
    {
        private readonly IEncrypter _encrypter;
        private readonly IMapper _mapper;
        //private readonly IUserRepository _userRepository;
        private readonly IMongoRepository<User> _userRepository;

        public UserService(/*IUserRepository userRepository, */IMongoRepository<User> userRepository, IEncrypter encrypter, IMapper mapper)
        {
            _userRepository = userRepository;
            _encrypter = encrypter;
            _mapper = mapper;
        }

        public async Task<IEnumerable<UserDto>> BrowseAsync()
        {
            var drivers = await _userRepository.FindAsync(user => true);

            return _mapper.Map<IEnumerable<User>, IEnumerable<UserDto>>(drivers);
        }

        public async Task<UserDto> GetAsync(string username)
        {
            var user = await _userRepository.FirstOrDefaultAsync(u => u.Username == username);

            return _mapper.Map<User, UserDto>(user);
        }

        public async Task RegisterAsync(Guid userId, string email,
            string username, string fullName, string password, UserRole role)
        {
            var user = await _userRepository.FirstOrDefaultAsync(u => u.Email == email);
            if (user != null)
            {
                throw new ServiceException(Exceptions.ErrorCodes.EmailInUse,
                    $"User with email: '{email}' already exists.");
            }

            var salt = _encrypter.GetSalt(password);
            var hash = _encrypter.GetHash(password, salt);
            user = new User(userId, email, username, fullName, role, hash, salt);
            await _userRepository.AddAsync(user);
        }

        public async Task RemoveAsync(Guid id)
        {
            await _userRepository.DeleteAsync(id);
        }

        public async Task UpdateAsync(UserDto user)
        {
            await _userRepository.UpdateAsync(_mapper.Map<UserDto, User>(user));
        }
    }
}
