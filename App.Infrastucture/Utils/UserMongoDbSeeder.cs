﻿using App.Common.Settings;
using App.Core.Domain;
using App.Infrastucture.Services;
using MongoDB.Driver;
using System;
using System.Threading.Tasks;

namespace App.Infrastucture.Utils
{
    public class UserMongoDbSeeder : MongoDbSeeder
    {

        private readonly IUserService _userService;
        public UserMongoDbSeeder(IMongoDatabase database, IUserService userService) : base(database)
        {
            _userService = userService;
        }

        protected override async Task<bool> CustomSeedAsync()
        {
            if (!await base.CustomSeedAsync())
            {
                await _userService.RegisterAsync(Guid.NewGuid(), "an@a.pl", "AnKm", "Ania_Nieznana", "secret", UserRole.Admin);
                await _userService.RegisterAsync(Guid.NewGuid(), "lk@a.pl", "LkKm", "Lenka_Nieznana", "secret", UserRole.User);
                await _userService.RegisterAsync(Guid.NewGuid(), "ew@a.pl", "EwKm", "Eleonora_Nieznana", "secret", UserRole.User);
                return true;
            }
            return false;
        }
    }
}
