﻿using App.Common.Types;
using App.Infrastucture.DTO;

namespace App.Infrastucture.Query
{
    public class LoadUserQuery : IQuery<UserDto> 
    {
        public LoadUserQuery()
        {
                
        }

        public LoadUserQuery(string username)
        {
            UserName = username;
        }

        public string UserName { get; private set; }
    }
}
