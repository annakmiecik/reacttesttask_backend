﻿using App.Common.CQRS.Query;
using App.Infrastucture.DTO;
using App.Infrastucture.Services;
using System.Threading.Tasks;

namespace App.Infrastucture.Query
{
    public class LoadUserQueryHandler : IQueryHandler<LoadUserQuery, UserDto>
    {
        private readonly IUserService _userService;
        public LoadUserQueryHandler(IUserService userService)
        {
            _userService = userService;
        }
        public async Task<UserDto> HandleAsync(LoadUserQuery query)
        {
            return await _userService.GetAsync(query.UserName);
        }
    }
}
