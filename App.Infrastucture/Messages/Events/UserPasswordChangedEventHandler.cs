﻿using App.Common.RabbitMq;
using App.Infrastucture.Commands;
using System;
using System.Threading.Tasks;

namespace App.Infrastucture.Messages.Events
{
    public class UserPasswordChangedEventHandler : IHandleEvent<UserPasswordChangedEvent>
    {
        public Task HandleAsync(UserPasswordChangedEvent @event)
        {
            return Task.CompletedTask;
        }

        public Task HandleAsync(UserPasswordChangedEvent @event, ICorrelationContext context)
        {
            return Task.CompletedTask;
        }
    }
}
