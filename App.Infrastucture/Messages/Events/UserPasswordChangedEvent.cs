﻿using App.Common.Messages;
using App.Infrastucture.Commands;
using System;

namespace App.Infrastucture.Messages.Events
{
    [MessageNamespace("identity")]
    public class UserPasswordChangedEvent : IEvent
    {
        public Guid UserId { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
