﻿using App.Common.CQRS.Commands;
using App.Common.Messages;
using App.Core.Domain;

namespace App.Infrastucture.Messages.Commands.User
{
    [MessageNamespace("identity")]
    public class CreateUserCommand : ICommand
    {
        public CreateUserCommand()
        {
            Salt = "secret";
            Password = "secret";
        }
        public string Email { get; set; }
        public string Salt { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public UserRole Role { get; set; }
    }
}
