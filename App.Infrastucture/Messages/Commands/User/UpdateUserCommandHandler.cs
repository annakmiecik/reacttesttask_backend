﻿using App.Common.CQRS.Commands;
using App.Common.RabbitMq;
using App.Infrastucture.DTO;
using App.Infrastucture.Services;
using AutoMapper;
using System;
using System.Threading.Tasks;

namespace App.Infrastucture.Messages.Commands.User
{
    public class UpdateUserCommandHandler : ICommandHandler<UpdateUserCommand>
    {
        private readonly IMapper _automapper;
        private readonly IUserService _userService;
        public UpdateUserCommandHandler(IUserService userService, IMapper automapper)
        {
            _automapper = automapper;
            _userService = userService;
        }
        public Task HandleAsync(UpdateUserCommand command)
        {
            throw new NotImplementedException();
        }

        public async Task HandleAsync(UpdateUserCommand command, ICorrelationContext context)
        {
            await _userService.UpdateAsync(_automapper.Map<UserDto>(command))
;        }
    }
}
