﻿using App.Common.CQRS.Commands;
using App.Common.Messages;
using App.Core.Domain;
using System;

namespace App.Infrastucture.Messages.Commands.User
{
    [MessageNamespace("identity")]
    public class UpdateUserCommand : ICommand
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string Username { get; set; }
        public UserRole Role { get; set; }

    }
}
