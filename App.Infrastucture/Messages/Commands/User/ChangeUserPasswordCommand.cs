﻿using App.Common.CQRS.Commands;
using App.Common.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Infrastucture.Messages.Commands.User
{
    [MessageNamespace("identity")]
    public class ChangeUserPasswordCommand : ICommand
    {
        public Guid UserId { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
