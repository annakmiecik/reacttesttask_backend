﻿using App.Common.CQRS.Commands;
using App.Common.RabbitMq;
using App.Infrastucture.Services;
using System.Threading.Tasks;

namespace App.Infrastucture.Messages.Commands.User
{
    public class LoginHandler : ICommandHandler<LoginCommand>
    {
        private readonly IIdentityService _identityService;
        public LoginHandler(IIdentityService identityService)
        {
            _identityService = identityService;
        }

        public async Task HandleAsync(LoginCommand command)
        {
            await _identityService.LoginAsync(command.login, command.password);
        }

        public async Task HandleAsync(LoginCommand command, ICorrelationContext context)
        {
            await _identityService.LoginAsync(command.login, command.password);
        }
    }
}
