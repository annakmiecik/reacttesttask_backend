﻿using App.Common.CQRS.Commands;
using App.Common.RabbitMq;
using App.Infrastucture.Messages.Events;
using App.Infrastucture.Services;
using AutoMapper;
using System;
using System.Threading.Tasks;

namespace App.Infrastucture.Messages.Commands.User
{
    public class ChangeUserPasswordCommandHandler : ICommandHandler<ChangeUserPasswordCommand>
    {
        private readonly IMapper _automapper;
        private readonly IIdentityService _identityService;
        private readonly IBusPublisher _busPublisher;

        public ChangeUserPasswordCommandHandler(IIdentityService identityService, IBusPublisher busPublisher, IMapper automapper)
        {
            _automapper = automapper;
            _busPublisher = busPublisher;
            _identityService = identityService;
        }

        public async Task HandleAsync(ChangeUserPasswordCommand command)
        {
            await _identityService.ChangePasswordAsync(command.UserId, command.CurrentPassword, command.NewPassword);
        }

        public async Task HandleAsync(ChangeUserPasswordCommand command, ICorrelationContext context)
        {
            await _identityService.ChangePasswordAsync(command.UserId, command.CurrentPassword, command.NewPassword);
            await _busPublisher.PublishAsync(_automapper.Map<UserPasswordChangedEvent>(command), context);
        }
    }
}
