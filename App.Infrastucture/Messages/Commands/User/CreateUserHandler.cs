﻿using App.Common.CQRS.Commands;
using App.Common.RabbitMq;
using App.Infrastucture.Services;
using System;
using System.Threading.Tasks;

namespace App.Infrastucture.Messages.Commands.User
{
    public class CreateUserHandler : ICommandHandler<CreateUserCommand>
    {
        private readonly IUserService _userService;
        public CreateUserHandler(IUserService userService)
        {
            _userService = userService;
        }

        public async Task HandleAsync(CreateUserCommand command)
        {
            await _userService.RegisterAsync(Guid.NewGuid(), command.Email, command.Username, command.FullName, command.Password, command.Role);
        }

        public async Task HandleAsync(CreateUserCommand command, ICorrelationContext context)
        {
            await _userService.RegisterAsync(Guid.NewGuid(), command.Email, command.Username, command.FullName, command.Password, command.Role);
        }
    }
}
