﻿using App.Common.CQRS.Commands;
using App.Common.Messages;

namespace App.Infrastucture.Messages.Commands.User
{
    [MessageNamespace("identity")]
    public class LoginCommand : ICommand
    {
        public string login { get; set; }
        public string password { get; set; }
    }
}
