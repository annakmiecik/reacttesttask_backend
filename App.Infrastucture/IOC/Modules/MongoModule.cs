﻿using App.Common.Settings;
using App.Infrastucture.Utils;
using Autofac;
using System.Reflection;

namespace App.Infrastucture.IOC.Modules
{
    public class MongoModule : Autofac.Module
    {
         protected override void Load(ContainerBuilder builder)
        {
            var assembly = typeof(MongoModule)
                .GetTypeInfo()
                .Assembly;

            builder.RegisterType<UserMongoDbSeeder>()
                .As<IMongoDbSeeder>()
                .InstancePerLifetimeScope();

        }
    }
}
