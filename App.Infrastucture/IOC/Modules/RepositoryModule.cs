﻿using App.Core.Repository;
using App.Infrastucture.Services;
using Autofac;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace App.Infrastucture.IOC.Modules
{
    public class RepositoryModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assembly = typeof(IRepository)
                .GetTypeInfo()
                .Assembly;

            builder.RegisterAssemblyTypes(assembly)
                   .Where(x => x.IsAssignableTo<IRepository>())
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();

           

            //var assemblies = BuildManager.GetReferencedAssemblies().Cast<Assembly>().Where(x => x.FullName.Contains("Soundyladder")).ToArray();
            //builder.RegisterAssemblyTypes(assemblies)
            //    .AsImplementedInterfaces()
            //    .InstancePerRequest();


        }
    }
}

