﻿using App.Infrastucture.Services;
using Autofac;
using System.Reflection;

namespace App.Infrastucture.IOC.Modules
{
    public class ServiceModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assembly = typeof(ServiceModule)
                .GetTypeInfo()
                .Assembly;

            builder.RegisterType<Encrypter>()
                   .As<IEncrypter>()
                   .SingleInstance();

            builder.RegisterAssemblyTypes(assembly)
                   .Where(x => x.IsAssignableTo<IService>())
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();

            //builder.RegisterType<JwtHandler>()
            //       .As<IJwtHandler>()
            //       .SingleInstance();
        }
    }
}
