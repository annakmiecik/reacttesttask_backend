﻿using App.Core.Domain;
using App.Infrastucture.DTO;
using App.Infrastucture.Messages.Commands.User;
using App.Infrastucture.Messages.Events;
using AutoMapper;

namespace App.Infrastucture.Mappings
{
    public static class AutoMapperConfig
    {
        public static IMapper Initialize()
        => new MapperConfiguration(cfg =>
        {
            cfg.CreateMap<User, UserDto>(MemberList.None)
            .ReverseMap()
            .ForPath(x=>x.Salt, m=>m.Ignore());
            //.ForMember(x=>x.Id, m=>m.MapFrom(p=>p.Id));
            cfg.CreateMap<ChangeUserPasswordCommand, UserPasswordChangedEvent>();
            cfg.CreateMap<UpdateUserCommand, UserDto>(MemberList.None);
            cfg.CreateMap<CreateUserCommand, UserDto>(MemberList.None);
            
            //Ignore unmapped
            //cfg.ForAllMaps((map, exp) => exp.ForAllOtherMembers(opt => opt.Ignore()));

        })
        .CreateMapper();
    }
}
